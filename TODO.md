# Projet blog

On peut créer des billets
Une catégorie contient 0 ou N billets
Un billet n'appartient qu'à une catégorie
Un billet est rédigé par 1 utilisateur
Un billet peut avoir 0 ou N commentaires

## Todo

Fos User
Trad
Commentaires 

## Entités

Post
* id
* title
* content
* created_at
* updated_at
* status
* category_id
* user_id

Category
* id
* name

User
* id
* username
* email

Comment
* id
* title
* content
* created_at
* updated_at
* post_id
