<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // Récupération des billets de blog qui ont un statut à 1
/*        $posts = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findAll();

        $testPost = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findOneByTitle('Mon premier message');

        $testPosts = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findByTitle('Mon premier message');*/

        /*$statusPosts = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findByStatus(true, ['createdAt' => 'DESC']);*/

        $posts = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findWithCategoryAndStatus();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            1
        );

        return $this->render('default/index.html.twig', [
            'posts' => $pagination,
        ]);
    }

    /**
     * @Route("/view/{id}", requirements={"id": "\d+"}, name="view_post")
     */
    public function viewPostAction(Request $request, Post $post)
    {
        $comment = new Comment();
        $comment->setPost($post);

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();

            $em = $this->get('doctrine')->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('view_post', ['id' => $post->getId()]);
        }

        return $this->render('default/view_post.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cat/{id}", requirements={"id": "\d+"}, name="view_category")
     */
    public function viewCategoryAction(Category $category)
    {
        return $this->render('default/view_category.html.twig', [
            'category' => $category,
        ]);
    }
}
